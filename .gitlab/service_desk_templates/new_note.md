# GCmobility Service Request Update

Issue Reference: %{ISSUE_ID}

You are receiving this e-mail, because your GCmobility Service Request issue %{ISSUE_PATH} has been updated:

%{NOTE_TEXT}

