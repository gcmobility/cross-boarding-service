# Thank you for your GCmobility support request! 

We are tracking your request as ticket %{ISSUE_ID}, and will respond as soon as we can. 

For more information about this service please refer to the [GCmobility Service Description](https://gcmobility.gitlab.io/pages/service-about.html) 

For more information about the GCmobility Initiative please refer to our [About Page](https://gcmobility.gitlab.io/pages/about-gcmobility.html)

Here are some links to resources to get a head start on solving your issues. What are you having issues with?

## Top issues faced by mobile workers:

* Need Help with [MyKey](https://docs.google.com/presentation/d/1lXzeoGss76uODJxg8oDXz5Uz-5CbX2wCpMa3-9dd0cA/edit#slide=id.p24)?
* Help with [Email](https://docs.google.com/presentation/d/1lXzeoGss76uODJxg8oDXz5Uz-5CbX2wCpMa3-9dd0cA/edit#slide=id.p25)?
 
We have some additional resources to help you get mobile and stay mobile:

* New to the Free Agent program? Check out The [Free Agent Toolkit](https://www.gcpedia.gc.ca/wiki/Free_Agents/Tool_Box) on GCpedia
* New to being a mobile worker? Check out the [Cross Boarding Manual For Mobile Workers](https://docs.google.com/presentation/d/1lXzeoGss76uODJxg8oDXz5Uz-5CbX2wCpMa3-9dd0cA/edit?usp=sharing)
    * Getting Ready to Move? Check out the [Before Cross Boarding](https://docs.google.com/presentation/d/1lXzeoGss76uODJxg8oDXz5Uz-5CbX2wCpMa3-9dd0cA/edit#slide=id.p11) Section of the manual
    * Are you in the middle of cross boarding and need some resources? Check out [During Cross Boarding](https://docs.google.com/presentation/d/1lXzeoGss76uODJxg8oDXz5Uz-5CbX2wCpMa3-9dd0cA/edit#slide=id.p19)
    * Have you completed crossboarding? Check out some [Ater Cross Boarding](https://docs.google.com/presentation/d/1lXzeoGss76uODJxg8oDXz5Uz-5CbX2wCpMa3-9dd0cA/edit#slide=id.p31) resources.

At GCmobility, we look forward to supporting your mobility.


Be Mobile!
