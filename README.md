# Cross Boarding Service

The GCmobility Cross Boarding Service Project is a prototype service for cross boarding.
The goal is to turn this service into an GC Enterprise wide Digital Service to support crossboarding activities across multiple HR mobility programs.


## Service Lines
The GCmobility Service is currently modeled on the following Cross Boarding Journey Map. 
Each step is considered a seprate service line, with all steps throughout the service composing the entirty of the service.

![CrossBoardingJourneyMap-2020_v1_](/uploads/f8ed9bea238d095322bc8fa1e2fcb751/CrossBoardingJourneyMap-2020_v1_.PNG)


## Service Request Process
The Service Request process may evolve quickly throughout the initial phases of this project. Check back here often for updated processes.


In order to submit a service request please follow these steps:

### Self Empowered Service Process

*  Reach out to your HOST IT service desk and at least log a ticket
*  If applicable, contact your home department and at least log a ticket.
*  Check the FAZZ Tool Kit on GCpedia [internal link]
*  Check the [Cross Boarding Manual](https://drive.google.com/file/d/1zpzGlShuhc7Nr5oZL98Oe2YyYQt-kuR7/view?usp=sharing)
*  Check the #techproblems channel on the Free Agent Slack instance (Login required)
*  Check the repository of previous issues already logged [here](https://gitlab.com/gcmobility/cross-boarding-service/-/issues)

### Consierge Service Process
If the previous steps did not solve your problem then welcome to the second part of our service:

* Create a GitLab.com account [here](https://gitlab.com/users/sign_in#register-pane)
* Login to Gitlab.com [here](https://gitlab.com/users/sign_in)
* Join the Cross Boarding Service project [here](https://gitlab.com/gcmobility/cross-boarding-service)
* Launch a new service issue [here]()


### Offline Requests
In the event that a user of this service is not able to access the GC network to access this service on GCcode, then the following alternative methods shall be used:

* #techproblems channel on Canada's Free Agents Slack
* GCmobility channel on GCcollab Message (Stay Tuned!)


## Service Partners
The HR mobility programs currently participating in this prototype experiment include:

* Canada's Free Agents
* Shared Services Canada 

## Service Model
The current service model for GCmobility is based on two concepts smashed together

* A self-empowerment model, where we connect you with the infomration and resources you need to drive crossboarding.
* A consierge type model, where we try to help you cross challenging barriers you can cross yourself


## Service Capacity
Currently this service is resourced on a part time, unofficial basis of .3 of an FTE.


## Service Standard
Due to lack of staffing or budget, our service standards on based on a "best effort" approach

## Service Feedback


## Join the service
If you would like to add your HR Mobility program to this pilot please [Contact Us](https://gcmobility.gitlab.io/pages/contact-us.html)
